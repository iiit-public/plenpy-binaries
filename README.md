# Plenpy Binaries
This project provides the binary files (e.g. images, light fields) needed for 
testing of the main [plenpy project](https://gitlab.com/iiit-public/plenpy).